
const AdmZip = require('adm-zip');
const fileManager = require('../utils/fileManager');
const toKebabCase = require('../utils/toKebabCase');

const zipUpload = async (req, res, next) => {
    const file = req.file;
    const problemName = req.body.problemName;
    const folderName = toKebabCase(problemName);

    const zip = new AdmZip(`${file.destination}/${file.originalname}`);
    const inputFolder = 'test-cases/' + folderName + '/input';
    const outputFolder = 'test-cases/' + folderName + '/output';
    
    
    await fileManager.createDirectory(inputFolder);
    await fileManager.createDirectory(outputFolder);

    // zip.extractAllTo('test-cases/' + folderName, true);
	var zipEntries = zip.getEntries(); // an array of ZipEntry records

	zipEntries.forEach(function(zipEntry) {
		if (zipEntry.entryName.endsWith('input/') && zipEntry.isDirectory) {
            zip.extractEntryTo(zipEntry.entryName, inputFolder, false, true);
		}
		if (zipEntry.entryName.endsWith('output/') && zipEntry.isDirectory) {
            zip.extractEntryTo(zipEntry.entryName, outputFolder, false, true);
		}
	});
    res.send('ok');
};

module.exports = {
    zipUpload
}
