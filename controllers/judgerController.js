const axios = require('axios');
const { judge } = require('../judge-engine/RunnerManager');

const run = async (req, res, next) => {
    console.log('--- running ---');
    const file = req.file;
    const problemId = req.body.problemId;
    const userId = req.body.userId;
    const language = req.body.language;

    console.log('problemId: ', problemId);
    const newSubmission = {
        problemId,
        userId,
        language,
        timeSubmitted: new Date().toISOString(),
        veredict: 'NA',
        status: 'running',
        runtime: null,
        sourceCode: file.filename
    }
    // console.log('newSubmission: ', newSubmission);
    let response;
    let problem;
    let submission;
    try {
        response = await axios.default.post('http://localhost:8000/api/submissions', newSubmission, {
            headers: {
                'Content-Type': 'application/json'
            }
        });
        submission = response.data.data;
        response = await axios.default.get(`http://localhost:8000/api/problems/${problemId}`);
        problem = response.data.data;
    } catch (error) {
        console.log('error: ', error);
    }

    const result = await judge(problem, language, submission.sourceCode, problem.title);
    // console.log('result XD: ', result);
    
    const patch = {
        veredict: result.displayVeredict,
        runtime: result.time,
        status: 'judged'
    }
    

    try {
        response = await axios.default.patch(`http://localhost:8000/api/submissions/${submission.id}`, patch, {
            headers: {
                'Content-Type': 'application/json'
            }
        });
    } catch (error) {
        console.log('error: ', error);
    }
    res.send('ok');
}


module.exports = {
    run
}