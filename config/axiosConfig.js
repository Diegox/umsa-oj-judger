import axios from 'axios'

const BackendInstance = axios.create({
    baseURL: 'http://localhost:8000/api',
    headers: {
        'content-type':'application/json',
    }
});

const apiCall = (url, data, headers, method) => axios({
  method,
  url: url,
  data,
  headers
})

module.exports = {
    BackendInstance,
    apiCall
}