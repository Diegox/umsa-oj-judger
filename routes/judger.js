const express = require('express');
const fileManager = require('../utils/fileManager')

const router = express.Router();

const fileUpload = require('../middleware/fileUpload');

const judgerController = require('../controllers/judgerController');

router.post('/judger/run', fileUpload, judgerController.run);

module.exports = router;
