const express = require('express');
const AdmZip = require('adm-zip');
const fileManager = require('../utils/fileManager')

const router = express.Router();

const zipUpload = require('../middleware/zipUpload');
const fileUpload = require('../middleware/fileUpload');

router.post('/test-cases/upload-zip', zipUpload, async (req, res, next) => {
    const file = req.file;
    const folderName = req.body.folderName;

    const zip = new AdmZip(`${file.destination}${file.originalname}`);

    await fileManager.createDirectory('test-cases/' + folderName);

    zip.extractAllTo('test-cases/' + folderName, true);
    res.send('ok');
});

router.post('/judge', fileUpload, (req, res, next) => {
    const file = req.file;
    console.log('file: ', file);
    res.send('running...');
});

module.exports = router;
