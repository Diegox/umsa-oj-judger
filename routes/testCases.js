const express = require('express');
const router = express.Router();

const testCasesController = require('../controllers/testCasesController');

const zipUpload = require('../middleware/zipUpload');

router.post('/test-cases/upload-zip', zipUpload, testCasesController.zipUpload);

module.exports = router;
