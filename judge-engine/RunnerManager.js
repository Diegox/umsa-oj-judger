const RunnerFactory = require('./RunnerFactory');
const { v1: uuid } = require('uuid');

const judge = async (problem, language, solution, testCases) => {

    const runner = RunnerFactory.create(language);
    const id = uuid();

    // prepare files
    await runner.prepareFiles(solution, id);
    let result;

    // compile
    try {
        result = await runner.compile(id);
    } catch(error) {
        return error;
    }

    // run
    try {
        result = await runner.run(testCases, problem, id);
    } catch (error) {
        console.log('error: ', error);
    }

    try {
        await runner.clean(id);
    } catch (error) {
        console.log('error: ', error);
    }

    return result;
}

module.exports = {
    judge
};


/**
1. prepare files
    - move solution to the run folder (/run-folder)
    if no error happens, then continue to step 2
2. compile (if required)
    if compile error happens, them report, and submit veredict
    else continue to step 3
3. run
    - run program test case by test case generating std output files and comparing with the test cases provided previously
        if some problem appears (WA, RE, TLE, etc), then stop and submit veredict
        else, run until all the test cases are judged
        then submic ok veredict
4. clean the /run-folder

 */
