class Runner {
    constructor(problem, language, solution, testCases) {
        this.problem = problem;
        this.language = language;
        this.solution = solution;
        this.testCases = testCases;
    }

    prepareFiles() {

    }

    compile() {

    }

    run() {

    }

    clean() {

    }
}

module.exports = Runner;