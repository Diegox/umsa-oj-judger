const CppRunner = require('./CppRunner');
const JavaRunner = require('./JavaRunner');
const PythonRunner = require('./PythonRunner')

class RunnerFactory {
    create(type) {
        switch (type.toLowerCase()) {
            case 'java':
                return new JavaRunner();
            case 'cpp':
                return new CppRunner();
            case 'python':
                return new PythonRunner();
            default: 
                console.log('Unknown language type...');
        }
    }
}

module.exports = new RunnerFactory();