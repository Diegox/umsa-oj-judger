const { spawn, exec, execSync } = require("child_process");
const Runner = require("./Runner");
const fileManager = require('../utils/fileManager');
const path = require('path');
const appRoot = require('app-root-path');
const toKebabCase = require('./../utils/toKebabCase');
const fs = require('fs');

const generateSuccessCompilationLog = (stderr = null, stdout = null, exitCode = null, killed = null) => {
  const result = 'Compiled succesfully';
  return {
    stderr,
    stdout,
    exitCode,
    killed,
    result 
  };
}

const generateCompilationErrorLog = (error) => {
  const result = 'Compilation error';
  let message = String(error.message || '');
  message = message.substring(message.indexOf('error: '));
  return {
    code: error.code,
    killed: error.killed,
    message,
    signal: error.signal,
    result
  };
}

const compareOutput = (folderName, caseNo, id) => {
  const originalTestCasesPath = `${appRoot}/test-cases/${folderName}/output/output${caseNo}.txt`;
  const solutionTestCasesPath = `${appRoot}/${id}/judge-output/output${caseNo}.txt`;
  try {
    const original = fs.readFileSync(originalTestCasesPath, 'utf-8');
    const solution = fs.readFileSync(solutionTestCasesPath, 'utf-8');  
    return original === solution;  
  } catch (error) {
    console.log('error: ', error);
  }
}
class CppRunner extends Runner {

  constructor(problem, language, solution, testCases) {
    super(problem, language, solution, testCases);
  }

  async prepareFiles(solution, id) {
    const sourceDirectory = path.resolve(`${appRoot}`, `solutions-folder`, solution);
    const destinationDirectory = path.resolve(`${appRoot}`, `${id}`, `run-folder`, 'Main.cpp');
    await fileManager.createDirectory(path.resolve(`${appRoot}`, `${id}`, 'judge-output'));
    await fileManager.copyFile(sourceDirectory, destinationDirectory)
  }

  compile(id) {
    return new Promise((resolve, reject) => {
      const options = {};
      const cmd = `g++ -o ${appRoot}/${id}/run-folder/Main ${appRoot}/${id}/run-folder/Main.cpp`;

      const response = {
        success: true,
        compilationError: false,
        veredict: '',
        time: -1,
        displayVeredict: '',
        totalTestCases: 0,
        judgedTestCases: 0,
        judgments: [],
        failedJudgment: {},
      }

      // return {
      //   code: error.code,
      //   killed: error.killed,
      //   message,
      //   signal: error.signal,
      //   result
      // };

      const gpp = exec(cmd, options, (error, stdout, stderr) => {
        let compilationResult;
        if (error) {
          compilationResult = generateCompilationErrorLog(error);
          response.compilationError = true;
          response.displayVeredict = 'Compilation Error';
          response.veredict = 'COMPILATION_ERROR';
          response.success = false;
          response.message = compilationResult.message;
          reject(response);
        }
        resolve(generateSuccessCompilationLog(stderr, stdout, gpp.exitCode, gpp.killed));
      });
    });
  }

  execute(cmd, options) { 
    return new Promise((resolve, reject) => {
      const process = exec(cmd, options, (error, stdout, stderr) => {
        if (error) {
          reject(error);
        }
        const result = {
          stdout,
          stderr,
          exitCode: process.exitCode,
        }
        resolve(result);
      });
    });
  }

  async run(testCases, problem, id) {
    const folderName = toKebabCase(testCases);
    const timeLimit = problem.timeLimit;
    const memoryLimit = problem.memoryLimit;
    const inputTestCasesFolder = `${appRoot}/test-cases/${folderName}/input`;

    const options = {
      encoding: 'utf8',
      shell: '/bin/bash',
      timeout: timeLimit * 1000,
      killSignal: 'SIGKILL',
      env: null
    };

    const testCaseFiles = fs.readdirSync(inputTestCasesFolder, {});
    const response = {
      success: true,
      compilationError: false,
      veredict: '',
      time: -1,
      displayVeredict: '',
      totalTestCases: testCaseFiles.length,
      judgedTestCases: 0,
      judgments: [],
      failedJudgment: {},
    }

    let caseNo = 1;
    for (const fileName of testCaseFiles) {
      let judgment = {};
      const cmd = `${appRoot}/${id}/run-folder/Main < ${inputTestCasesFolder}/${fileName} > ${appRoot}/${id}/judge-output/output${caseNo}.txt`;
      const startTime = Date.now();
      try {
        // success?
        await this.execute(cmd, options);
        
        const endTime = Date.now();
        const totalTime = (endTime - startTime) / 1000.0;
        judgment.time = totalTime;
        
        const accepted = compareOutput(folderName, caseNo, id);
        if (accepted) {
          judgment.veredict = 'ACCEPTED';
          judgment.displayVeredict = 'Accepted';
          response.judgments.push(judgment);
        } else {
          judgment.veredict = 'WRONG_ANSWER';
          judgment.displayVeredict = 'Wrong Answer';
          response.success = false;
          response.veredict = judgment.veredict;
          response.displayVeredict = judgment.displayVeredict;
          response.failedJudgment = judgment;
          response.judgedTestCases = caseNo;
          break;
        }
      } catch (error) {
        // error
        const endTime = Date.now();
        const totalTime = (endTime - startTime) / 1000.0;
        judgment.time = totalTime;

        if (error.killed && error.signal === 'SIGKILL' && error.code === null) {
          judgment.exitCode = 256;
          judgment.veredict = judgment.displayVeredict = 'TLE';
        } else if (judgment.exitCode === 136) {
          judgment.veredict = 'SIGFPE';
          judgment.displayVeredict = 'RTE(SIGFPE)';
        } else if (judgment.exitCode === 139) {
          judgment.veredict = 'SIGSEGV';
          judgment.displayVeredict = 'RTE(SIGSEGV)';
        } else if (judgment.exitCode === 153) {
          judgment.veredict = 'SIGXFSZ';
          judgment.displayVeredict = 'RTE(SIGXFSZ)';
        } else if (judgment.exitCode == 134) {
          judgment.veredict = 'SIGABRT';
          judgment.displayVeredict = 'RTE(SIGABRT)';
        } else {
          judgment.veredict = 'NZEC';
          judgment.displayVeredict = 'RTE(NZEC)';
        }
        response.success = false;
        response.veredict = judgment.veredict;
        response.displayVeredict = judgment.displayVeredict;
        response.failedJudgment = judgment;
        response.judgedTestCases = caseNo;
        break;
      }
      caseNo = caseNo + 1;
    }
    if (response.success) {
      response.veredict = 'ACCEPTED';
      response.displayVeredict = 'Accepted';
      response.time = response.judgments.map(judgment => judgment.time).reduce((prev, current) => Math.max(prev, current), -1);
      response.judgedTestCases = caseNo - 1;
    }
    return response;
  }
  
  async clean(id) {
    await fileManager.delete(path.resolve(`${appRoot}`, `${id}`))
  }

}

module.exports = CppRunner;
