const { copy, mkdir, remove } = require('./files');

module.exports = {
    async createDirectory(path) {
        try {
            const res = await mkdir(path);
            console.log('res: ', res);
        } catch (error) {
            throw new Error(error);
        }        
    },
    async copyFile(source, target) {
        try {
            const res = await copy(source, target);
            console.log('res: ', res);
        } catch (error) {
            throw new Error(error);            
        }
    },
    async delete(path) {
        try {
            const res = await remove(path);
            console.log('delete: ', res);
        } catch (error) {
            throw new Error(error);            
        }
    }    
}