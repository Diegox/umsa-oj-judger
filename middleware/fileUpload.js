const multer = require('multer');
const { v1: uuid } = require('uuid');

const MIME_TYPE_MAP = {
    'text/x-java': 'java',
    'text/x-python': 'py',    
    'text/x-c++src': 'cpp'
};

const fileUpload = multer({
    limits: 500000,
    storage: multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, 'solutions-folder/');
        },
        filename: (req, file, cb) => {
            const ext = MIME_TYPE_MAP[file.mimetype];
            cb(null, uuid() + '.' + ext);
        }
    })
}).single('solution');

module.exports = fileUpload;