const multer = require('multer');

const MYME_TYPE_MAP = {
    'application/zip': 'zip'
};

const zipUpload = multer({
    limits: 500000,
    storage: multer.diskStorage({
        destination: (req, file, cb) => {
            cb(null, 'temp-zip-folder/');
        },
        filename: (req, file, cb) => {
            cb(null, file.originalname);
        }
    }),
    fileFilter: (req, file, cb) => {
        const isValid = !!MYME_TYPE_MAP[file.mimetype];
        let error = isValid ? null : new Error('Invalid type!');
        cb(error, isValid);
    }
}).single('testCases');

module.exports = zipUpload;