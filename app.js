const createError = require('http-errors');
const express = require('express');
const logger = require('morgan');
const cors = require('cors');

const testCasesRouter = require('./routes/testCases');
const judgerRouter = require('./routes/judger');


const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cors());

app.use('/api', testCasesRouter);
app.use('/api', judgerRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
// app.use(function(err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err;
//   res.locals.error = req.app.get('env') === 'development' ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   res.send('404 Not found');
// });

module.exports = app;
